<?php include('header.php'); ?>


<div class="projects-about">
    <div class="container">
        <div class="title">
            <h1>О Компании</h1>
        </div>
        <div class="projects-discription about-discription text-center">
            <p>Компания «PROFSERVICE» поставщик IT-решений и успешный системный интегратор в отрасли информационных и
                аудиовизуальных технологий, специализирующийся на услугах разработки, внедрения и реализации
                инновационных проектов, на территории Республики Казахстан и стран СНГ.</p>
            <p>Основной задачей Компании является создание и реализация устойчивых инженерных решений, способных
                эффективно и в долгосрочной перспективе служить Заказчику надежным инструментом для реализации
                поставленных целей.</p>
        </div>
    </div>
    <div class="about-content">
        <div class="about-block">
            <div class="row w-100 m-0">
                <div class="col-xl-5 col-lg-6">
                    <div class="container">
                        <div class="about-left-block">
                            <h2>Сотрудники ТОО «PROFSERVICE» </h2>
                            <p>
                                имеют высокую квалификацию, что подтверждается соответствующими <b>дипломами и
                                    сертификатами</b>, а также имеют опыт в разработке и реализации проектов разного
                                уровня сложности. В Компании внедрены <b>современные методы</b> и <b>технологии
                                    менеджмента</b>, что делает нашу работу эффективной и ориентированной на Клиента.
                                Чтобы непрерывно идти в ногу со временем, много внимания мы уделяем инновациям и новым
                                высокотехнологичным профессиональным решениям. А также поддерживаем развитие
                                <b>профессиональных и личных навыков</b> у сотрудников.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 p-0">
                    <div class="about-right-block">
                        <div class="about-right-head d-flex">
                            <div class="about-right-content align-items-center text-center">
                                <img src="./images/univer-right-1.png" alt="">
                                <h5>квалификация, подтвержденная дипломамы и сертификатами</h5>
                            </div>
                            <div class="about-right-content  align-items-center text-center">
                                <img src="./images/univer-right-2.png" alt="">
                                <h5>современные методы и технологии менеджмента</h5>
                            </div>
                            <div class="about-right-content  align-items-center text-center">
                                <img src="./images/img-razvitie.png" alt="">
                                <h5>развитие профессиональных и личных
                                    навыков</h5>
                            </div>
                        </div>
                        <button class="btn btn-danger btn-service mt-4"><span class="mr-2"><img src="./images/email.png" alt=""></span> стать нашим клиентом!</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="companies-portfolio">
        <div class="container">
            <div class="title-portfolio">
                <h2>В портфеле Компании</h2>
            </div>
            <span class="about-bg"></span>
            <div class="portfolio-content">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="companies-logo">
                            <div class="row">
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-2.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-3.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-4.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-5.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-6.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img class="bosch_logo" src="./images/bitmap-7.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-8.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-4 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-9.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-4 col-6 col-md-6 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-10.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-3 p-0">
                                    <div class="logo">
                                        <img src="./images/bitmap-11.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="portfolio-text">
                            <p>В портфеле Компании <b>более 20 партнеров</b> — мировых лидеров в сфере производства
                                высокотехнологичного оборудования и программного обеспечения, среди которых Cisco,
                                Christie
                                Digital, HP, Microsoft, AXIS, Bosch, Geovision, Huawei, Fortinet, HRS, nuPSYS и другие.
                                <br><br>
                                Индивидуальный подход и оценка специфики предприятия Заказчика, позволяет Компании
                                создавать
                                уникальные комплексные и инфраструктурные решения практически в любой сфере
                                информатизации
                                бизнеса.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="major-companies">
        <div class="container">
            <div class="title">
                <h1>Основные направления Компании: </h1>
            </div>
            <div class="projects-discription about-discription text-center">
                <p>Решения ТОО «PROFSERVICE» используется на объектах государственной сферы, культуры, здравоохранения,
                    образования, финансовой и коммерческой сферы.
                    Наши преимущества в том, что мы предлагаем комплексные решения по автоматизации технологических и
                    бизнес-процессов.
                </p>
            </div>
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-1.png" alt="">
                        </div>
                        <h5>Аддитивные технологии</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-2.png" alt="">
                        </div>
                        <h5>Аудио- визуальное оборудование</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-3.png" alt="">
                        </div>
                        <h5>Аудио- и видеоконференц связь</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-4.png" alt="">
                        </div>
                        <h5>Голосовая коммутация</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-5.png" alt="">
                        </div>
                        <h5>Инфраструктура ЦОД</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-6.png" alt="">
                        </div>
                        <h5>Инфраструктурное печатно-копировальное оборудование</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-7.png" alt="">
                        </div>
                        <h5>Оборудование для передачи данных</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-8.png" alt="">
                        </div>
                        <h5>Оборудование радиочастотной идентификации (RFID, РЧИД)</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-9.png" alt="">
                        </div>
                        <h5>Оборудование систем безопасности</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-10.png" alt="">
                        </div>
                        <h5>Оборудование электропитания</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-11.png" alt="">
                        </div>
                        <h5>Программное обеспечение</h5>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-4">
                    <div class="blue-logo text-center">
                        <div class="blue-img">
                            <img src="./images/com-12.png" alt="">
                        </div>
                        <h5>Серверное оборудование</h5>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-danger btn-service">Полный список услуг</button>
    </div>

    <div class="container">
        <div class="vacancies">
            <div class="title">
                <h1>Открытые вакансии</h1>
            </div>
            <div class="owl-carousel owl-theme vacancies-slider">

                <div class="item">
                    <h5>Lorem ipsum dolor sit amet.</h5>
                    <br>
                    <div class="vacancies-title">
                        <p><b>№-1 </b><span class="red-text">Lorem ipsum dolor sit, amet consectetur adipisicing.</span></p>
                    </div>
                    <div class="vacancies-first-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque tempora quae voluptatibus omnis expedita odio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <a class="red-text textClick" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Развернуть
                    </a>
                    <div class="vacancies-last-text collapse" id="collapseExample">
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <button class="btn btn-outline-danger btn-outline">Подать заявку</button>
                </div>
                <div class="item">
                    <h5>Lorem ipsum dolor sit amet.</h5>
                    <br>
                    <div class="vacancies-title">
                        <p><b>№-1 </b><span class="red-text">Lorem ipsum dolor sit, amet consectetur adipisicing.</span></p>
                    </div>
                    <div class="vacancies-first-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque tempora quae voluptatibus omnis expedita odio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <a class="red-text textClick" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Развернуть
                    </a>
                    <div class="vacancies-last-text collapse" id="collapseExample">
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <button class="btn btn-outline-danger btn-outline">Подать заявку</button>
                </div>
                <div class="item">
                    <h5>Lorem ipsum dolor sit amet.</h5>
                    <br>
                    <div class="vacancies-title">
                        <p><b>№-1 </b><span class="red-text">Lorem ipsum dolor sit, amet consectetur adipisicing.</span></p>
                    </div>
                    <div class="vacancies-first-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque tempora quae voluptatibus omnis expedita odio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <a class="red-text textClick" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Развернуть
                    </a>
                    <div class="vacancies-last-text collapse" id="collapseExample">
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <button class="btn btn-outline-danger btn-outline">Подать заявку</button>
                </div>
                <div class="item">
                    <h5>Lorem ipsum dolor sit amet.</h5>
                    <br>
                    <div class="vacancies-title">
                        <p><b>№-4 </b><span class="red-text">Lorem ipsum dolor sit, amet consectetur adipisicing.</span></p>
                    </div>
                    <div class="vacancies-first-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque tempora quae voluptatibus omnis expedita odio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <div class="vacancies-last-text">
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>L. vodio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <button class="btn btn-outline-danger btn-outline">Подать заявку</button>
                </div>
                <div class="item">
                    <h5>Lorem ipsum dolor sit amet.</h5>
                    <br>
                    <div class="vacancies-title">
                        <p><b>№-1 </b><span class="red-text">Lorem ipsum dolor sit, amet consectetur adipisicing.</span></p>
                    </div>
                    <div class="vacancies-first-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque tempora quae voluptatibus omnis expedita odio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <a class="red-text textClick" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Развернуть
                    </a>
                    <div class="vacancies-last-text collapse" id="collapseExample">
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                        <b>Lorem ipsum dolor sit amet.</b>
                        <p>Lo Evolio iure, reiciendis iste non, quo repudiandae possimus harum delectus soluta atque eveniet illo beatae numquam?</p>
                    </div>
                    <button class="btn btn-outline-danger btn-outline">Подать заявку</button>
                </div>
            </div>
            <a href="#" class="vacancies-left">
                <span>
                    <img src="./images/left-client.png" alt="">
                </span>
            </a>
            <a href="#" class="vacancies-right">
                <span>
                    <img src="./images/right-client.png" alt="">
                </span>
            </a>
        </div>
    </div>
</div>







<?php include('footer.php'); ?>
