<?php include('header.php'); ?>


<div class="projects">
    <div class="container">
        <div class="title">
            <h1>Наши бренды</h1>
        </div>
        <div class="projects-discription text-center">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet aLorem ipsum dolor sit amet,
                consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>
        </div>
    </div>
</div>
<div class="brands">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-1.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-2.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-3.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-4.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-5.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-6.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-7.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-8.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-9.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-10.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-11.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-12.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-13.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-14.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-15.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-16.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-17.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-18.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-19.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-20.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-21.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-22.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-23.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-24.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-25.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-26.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-27.png" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-4 col-12">
                <div class="brand-content">
                    <img src="./images/brand-28.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>




<?php include('footer.php'); ?>