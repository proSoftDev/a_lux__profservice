<?php include('header.php'); ?>
<div class="container">
    <div class="contacts-page">
        <div class="title">
            <h1>Контакты</h1>
        </div>
        <div class="contacts-content text-center col-xl-7 m-auto">
            <b> ТОО «ProfService», </b>
            <p>
                Республика Казахстан, 010000, г. Нур-Султан, район Есиль
                ул. Динмухамеда Кунаева, 29/1, 20 этаж, оф. 2008,
            </p>
            <br>
           <span>e-mail: </span><a href="#">info@profservice.team</a>
           <br>
           <a href="#" type="button" class="btn btn-danger btn-service"><img src="./images/email.png" alt=""> стать нашим клиентом!</a>
            <div class="social-contact">
                <p>Мы в социальных сетях:</p>
                <div class="social-icon d-flex align-items-center justify-content-center">
                    <a href="#"><img src="./images/icon-twitter.png" alt=""></a>
                    <a href="#"><img src="./images/icon-youtube.png" alt=""></a>
                    <a href="#"><img src="./images/icon-linkedin.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>

</div>





<?php include('footer.php'); ?>