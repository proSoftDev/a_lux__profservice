$(document).ready(function () {

    $('.mobile-menu .burger-menu-btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('burger-menu-lines-active');
        $('.mobile-nav').toggleClass('mobile-nav-active');
    });

    let projectSlider = $('.project-slider');
    projectSlider.owlCarousel({
        loop: true,
        dots: true,
        arrows: false,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    let projectNameSlider = $('.project-name-slider');
    projectNameSlider.owlCarousel({
        loop: true,
        dots: false,
        arrows: false,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    $('.arrow-left').click(function (e) {
        e.preventDefault();
        projectNameSlider.trigger('prev.owl.carousel');
    });
    $('.arrow-right').click(function (e) {
        e.preventDefault();
        projectNameSlider.trigger('next.owl.carousel');
    })

    let clientsSlider = $('.clients-slider');
    clientsSlider.owlCarousel({
        loop: true,
        dots: false,
        arrows: false,
        margin: 30,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            1000: {
                items: 4
            }
        }
    });
    $('.clients-left').click(function (e) {
        e.preventDefault();
        clientsSlider.trigger('prev.owl.carousel');
    });
    $('.clients-right').click(function (e) {
        e.preventDefault();
        clientsSlider.trigger('next.owl.carousel');
    });

    let vacanciesSlider = $('.vacancies-slider');
    vacanciesSlider.owlCarousel({
        loop: true,
        dots: false,
        arrows: false,
        autoplay: true,
        margin: 30,
        center: true,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
    $('.vacancies-left').click(function (e) {
        e.preventDefault();
        vacanciesSlider.trigger('prev.owl.carousel');
    });

    $('.vacancies-right').click(function (e) {
        e.preventDefault();
        vacanciesSlider.trigger('next.owl.carousel');
    })
    $('.textClick').click(function () {
        console.log('afkafo');
        if (this.innerHTML == "Свернуть") {
            this.innerHTML = "Развернуть";
        } else {
            this.innerHTML = "Свернуть";
        }
    });
    $('.btn-order').mouseenter(function () {
        $(this).parent().addClass('border-card');
    });
    $('.btn-order').mouseleave(function () {
        $('.services-card').removeClass('border-card');
    });
});