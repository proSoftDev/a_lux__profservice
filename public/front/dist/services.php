<?php include('header.php'); ?>

<div class="services">
    <div class="services-discription text-center">
        <div class="container">
            <div class="title">
                <h1>Услуги</h1>
            </div>
            <p>Компания «PROFSERVICE» поставщик IT-решений и успешный системный интегратор в отрасли информационных и
                аудиовизуальных технологий, специализирующийся на услугах разработки, внедрения и реализации
                инновационных проектов, на территории Республики Казахстан и стран СНГ.</p>
            <p><b>Основной задачей</b> Компании является создание и реализация устойчивых инженерных решений, способных
                эффективно и в долгосрочной перспективе служить Заказчику надежным инструментом для реализации
                поставленных целей.</p>
        </div>
    </div>


    <div class="services-content">
    <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/print.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/outstaffing.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/project-key.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/diagnostics.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/consutling.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-3-d.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-expertice.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/project-infrustructure.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-montage-3-rd.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-software.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-servicesupport.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-govsupport.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-projectmanagement.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг офисной печати, аудит и сервисное обслуживание офисной
                                    печатно-множительной
                                    техники</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-12">
                    <div class="services-card">
                        <div class="services-card-header d-flex align-items-start">
                            <img src="./images/img-3-dprinting-service.png" alt="">
                            <div class="services-header-text">
                                <h5>Аутсорсинг функций и аутстаффинг персонала для непрофильных видов деятельности</h5>
                            </div>
                        </div>
                        <div class="services-card-text">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies </p>
                        </div>
                        <button type="button" class="btn btn-outline-danger btn-outline btn-order">заказать</button>
                    </div>
                </div>
            </div>
            <div class="services-btn text-center">
                <button type="button" class="btn btn-outline-danger btn-service">не нашли нужной услуги? напишите нам!</button>
            </div>
        </div>
    </div>
</div>







<?php include('footer.php'); ?>