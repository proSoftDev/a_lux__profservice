<?php

namespace App\Providers;

use App\Page\Page;
use App\Page as PageModel;
use App\Meta\MetaTag;
use Illuminate\Support\ServiceProvider;

class MetaTagsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    protected $request;
    protected $page;

    public function register()
    {
        $this->app->singleton(MetaTag::class, function ($app) {
            return new MetaTag(config('app.meta'));
        });

        $this->app->singleton(Page::class, function ($app) {
            return new Page();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(MetaTag $meta, Page $page)
    {
        $uri = request()->getRequestUri();
        $pageInstance = PageModel::where('url', $uri)->first();
        if($pageInstance) {
            $page->setPage($pageInstance);
            if($pageInstance->meta_title && $pageInstance->meta_description) {
                $meta->setTitle($pageInstance->meta_title);
                $meta->setDescription($pageInstance->meta_description);
            }
        }
        $page->setMeta($meta);
    }
}
