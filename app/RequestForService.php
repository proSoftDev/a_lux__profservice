<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RequestForService extends Model
{
    protected $fillable = ['name', 'telephone', 'service_id'];

    public function service(){
        return $this->belongsTo(Service::class);
    }

}
