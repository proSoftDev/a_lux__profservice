<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RequestForProject extends Model
{
    protected $fillable = ['name', 'project_id', 'telephone'];

    public function project(){
        return $this->belongsTo(Project::class);
    }
    
}
