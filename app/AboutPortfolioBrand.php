<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AboutPortfolioBrand extends Model
{
    public static function getAll(){
        return AboutPortfolioBrand::get();
    }
}
