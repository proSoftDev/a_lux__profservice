<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Banner extends Model
{
    use Translatable;
    protected $translatable = ['title','content'];

    public static function getAll(){
        return Banner::orderBy('sort', 'ASC')->get();
    }
}
