<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutStaff extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];

    public static function getContent(){
        return AboutStaff::find(1);
    }
}
