<?php

namespace App\Page;

use App\Meta\MetaTag;
use App\Page as PageModel;
use Illuminate\Support\Facades\App;
use App\Helpers\TranslatesCollection;

class Page {

    private $meta;
    private $page;


    public function setPage(PageModel $page) {
        $this->page = $page;
    }


    public function setMeta(MetaTag $meta) {
        $this->meta = $meta;
    }

    public function getPage() {
        return $this->page;
    }

    public function getMeta() {
        return $this->meta;
    }

}
