<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;



class TitlePage extends Model
{
    const about = 2;
    const project = 3;
    const service = 4;
    const brand = 5;
    const contact = 6;

    use Translatable;
    protected $translatable = ['title','content'];

    public static function getAboutContent(){
        return TitlePage::where("menu_id", self::about)->first();
    }

    public static function getProjectContent(){
        return TitlePage::where("menu_id", self::project)->first();
    }

    public static function getServiceContent(){
        return TitlePage::where("menu_id", self::service)->first();
    }

    public static function getBrandContent()
    {
        return TitlePage::where("menu_id", self::brand)->first();
    }

    public static function getContactContent()
    {
        return TitlePage::where("menu_id", self::contact)->first();
    }
    
}
