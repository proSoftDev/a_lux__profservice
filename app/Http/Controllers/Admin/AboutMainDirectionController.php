<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22.01.2020
 * Time: 11:53
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutMainDirectionController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
