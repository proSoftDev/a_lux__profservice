<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.01.2020
 * Time: 10:07
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\Service;
use App\TitlePage;


class ServicesController extends Controller
{
    public function Index(){

        $model = TitlePage::getServiceContent();
        $services = Service::getAll();

        TranslatesCollection::translate($model, app()->getLocale());
        TranslatesCollection::translate($services, app()->getLocale());
        return view('services.index', compact('model','services'));
    }

}
