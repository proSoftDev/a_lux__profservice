<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.01.2020
 * Time: 10:08
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\TitlePage;

class ContactsController extends Controller
{
    public function Index(){

        $model = TitlePage::getContactContent();
        TranslatesCollection::translate($model, app()->getLocale());

        return view('contacts.index', compact('model'));
    }

}
