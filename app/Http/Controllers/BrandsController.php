<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21.01.2020
 * Time: 10:07
 */

namespace App\Http\Controllers;


use App\Brand;
use App\Helpers\TranslatesCollection;
use App\TitlePage;

class BrandsController
{
    public function Index(){
        $model = TitlePage::getBrandContent();
        $brands = Brand::getAll();
        TranslatesCollection::translate($model, app()->getLocale());

        return view('brands.index', compact('model','brands'));
    }

}
