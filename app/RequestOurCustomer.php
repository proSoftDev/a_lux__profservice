<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RequestOurCustomer extends Model
{
    protected $fillable = ['fio', 'company', 'face', 'telephone'];
}
