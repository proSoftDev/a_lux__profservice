<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'читать подробнее' => 'read more',
    'стать нашим клиентом!' => 'become our customer!',
    'Полный список услуг' => 'Full list of services',
    'Открытые вакансии' => 'Open vacancies',
    'З/П:' => 'Salary:',
    'Требования' => 'Requirements',
    'Соцпакет' => 'Social package',
    'Подать заявку' => 'Apply',
    'ТОО «ProfService»' => 'LLP «ProfService»',
    'Мы в социальных сетях' => 'We are in social networks',
    'Последние проекты' => 'Recent projects',
    'Наши клиенты' => 'Our clients',
    'Наши услуги' => 'Our services',
    'Свяжитесь с нами' => 'Contact us',
    'Ваше имя' => 'Your name',
    'Email' => 'Email',
    'Комментарии' => 'Comments',
    'Ваше сообщение' => 'Your message',
    'Отправить' => 'Submit',
    'Консультационные и практические услуги в области информационных технологий' => 'Consulting and practical services in the field of information technology',
    'Станьте нашим клиентом' => 'Become our customer',
    'ФИО' => 'Full name',
    'Имя' => 'Name',
    'Компания' => 'Company',
    'Лица' => 'Person',
    'Физического Лица' => 'Physical person',
    'Юридического Лица' => 'Legal person',
    'Телефон' => 'Telephone',
    'Заказать услугу' => 'Order service',
    'Заказать' => 'Order',
    'Заказать подобный проект' => 'Order a similar project',
    'заказать подобный проект' => 'order a similar project',
    'Например' => 'For example',
    'не нашли нужной услуги? напишите нам!' => 'did not find the right service? write to us!',
    'Ваша заявка принята' => 'Your application is accepted!',
];
