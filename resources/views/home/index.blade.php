@extends('layouts.main')
@section('content')

    <div class="mian-slider">
        <div class="owl-carousel project-slider owl-theme">
            @foreach($banners as $k => $v)
                <div class="item" style="background-image: url({{ asset('storage/'.$v->image )}});height:720px;">
                    <!-- <img src="{{ asset('storage/'.$v->image )}}" alt=""> -->
                    <div class="container">
                        <div class="main-text">
                            <h1>{!! $v->title !!}</h1>
                            <p>{!! $v->content !!}</p>
                            <a href="{{ $v->url}}"><button class="btn btn-danger btn-service">@lang('messages.читать подробнее')</button></a>   
                            
                        
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- ABOUT-MAIN -->
    <div class="about-main text-center">
        <span class="about-bg"></span>
        <div class="container">
            <div class="title">
                <h1>{{ $about->title }}</h1>
            </div>
            <div class="about-text" data-aos="fade-up">
                <p>{!! $about->content !!}</p>
            </div>
            <div class="about-content">
                <div class="row justify-content-center">
                    @php $m=0; @endphp
                    @foreach($histories as $k => $v)
                        @php $m++; @endphp
                        @if($m % 2 == 1)
                            <div class="col-xl-5 p-0"><div></div></div>
                            <div class="col-xl-1 line"></div>
                            <div class="col-xl-5 p-0">
                                <div class="about-right" data-aos="fade-right">
                                    <div class="date">
                                        <h4>{{ $v->year}}</h4>
                                    </div>
                                    <div class="right-text">
                                        <p>{{ $v->content}}</p>
                                    </div>
                                </div>
                            </div>
                        @elseif($m % 2 == 0)
                            <div class="col-xl-5 p-0">
                                <div class="about-left" data-aos="fade-left">
                                    <div class="date">
                                        <h4>{{ $v->year}}</h4>
                                    </div>
                                    <div class="left-text">
                                        <p>{{ $v->content}}</p>
                                    </div>
                                </div>
                            </div>
                            @if($m!=count($histories))
                                <div class="col-xl-1 line"></div>
                                <div class="col-xl-5"><div></div></div>
                            @endif
                        @endif
                    @endforeach

                    <div class="col-xl-1 end-line"></div>
                    <div class="col-xl-5"><div></div></div>

                    <!-- <button class="btn btn-danger btn-service mt-4" data-toggle="modal" data-target="#requestClientModal"><span class="mr-2"><img src="./images/email.png" alt="">
                        </span>@lang('messages.стать нашим клиентом!') </button> -->
                </div>
            </div>
        </div>
    </div>

    <!-- ABOUT-MAIN-END -->

    <!-- OUR-SERVICE -->
    <div class="our-service text-center">
        <div class="container">
            <div class="title">
                <h1>@lang('messages.Наши услуги')</h1>
            </div>
            <div class="row justify-content-center">
                @foreach($services as $v)
                    <div class="col-xl-3 col-md-6" data-aos="fade-up">
                        <div class="our-card">
                            <div class="blue-img">
                                <img src="{{ asset('storage/'.$v->main_icon)}}" alt="{{$v->name}}">
                            </div>
                            <h5>{{$v->name}}</h5>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- <a class="btn btn-danger btn-service"  href="{{route('services')}}">@lang('messages.Полный список услуг')</a> -->
        </div>
    </div>

    <!-- OUR-SERVICE-END -->

    <!-- LAST-PROJECTS -->

    <div class="last-project">
        <div class="container">
            <div class="title">
                <h1>@lang('messages.Последние проекты')</h1>
            </div>
            <div class="owl-carousel owl-theme text-center project-slider">
                    @foreach($main_project as $k => $v)
                    <div class="item">
                        <p>{{ $v->content }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- LAST-PROJECTS-END -->

    <!-- PROJECT-NAME -->

    <div class="project-name">
        <div class="arrow-left">
            <a href="#"><img src="./images/arrow-left.png" alt=""></a>
        </div>
        <div class="arrow-right">
            <a href="#"><img src="./images/arrow-right.png" alt=""></a>
        </div>
        <div class="owl-carousel owl-theme project-name-slider">
            @foreach($projects as $k => $v)
                <div class="item">
                    <div class="col-xl-6 p-0 text-right">
                        <div class="project-name-item">
                            <div class="container">
                                <div class="name">
                                    <a href="/projects#p{{$v->id}}"><h2>{{ $v->name }}</h2></a>
                                </div>
                                <div class="project-name-icon d-flex justify-content-end">
                                <span>
                                    <img src="./images/ic-design.png" alt="">
                                </span>
                                    <span>
                                    <img src="./images/ic-development.png" alt="">
                                </span>
                                </div>
                                <p>{{ $v->content }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    <!-- CLIENTS -->
    <div class="clients">
        <div class="container">
            <div class="title">
                <h1>@lang('messages.Наши клиенты')</h1>
            </div>
            <a href="#" class="clients-left"><span><img src="./images/left-client.png" alt=""></span></a>
            <a href="#" class="clients-right"><span><img src="./images/right-client.png" alt=""></span></a>
            <div class="owl-carousel owl-theme clients-slider">
                @foreach($clients as $k => $v)
                    <div class="item" data-aos="flip-left">
                        <div class="clients-card text-center">
                            <div class="clients-img">
                                <img src="{{ asset('storage/'.$v->image)}}" alt="">
                            </div>
                            <div class="clients-card-text">
                                <p>{{ $v->content }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- CLIENTS-END -->




@endsection
