<div class="navbar navbar-expand-lg navbar-light p-0">
    <a class="navbar-brand" href="/"><img src="./images/logo-ps.png" alt=""></a>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <ul class="d-flex p-0 m-0">
                <li>
                    <p><a href="tel:{{ setting('site.telephone') }}">{{ setting('site.telephone') }}</a></p>
                </li>
                <li><a class="nav-item" href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></li>
            </ul>
            <div class="lang d-flex">
                @if(app()->getLocale() != null)
                    <a href="/lang/ru" class="{{ app()->isLocale("ru") ? ' lang-active' : '' }}">ru</a>
                    <a href="/lang/kz" class="{{ app()->isLocale("kz") ? ' lang-active' : '' }}">kz</a>
                    <a href="/lang/en" class="{{ app()->isLocale("en") ? ' lang-active' : '' }}">en</a>
                @else
                    <a href="/lang/ru" class="lang-active">ru</a>
                    <a href="/lang/kz">kz</a>
                    <a href="/lang/en">en</a>
                @endif
            </div>
        </div>
    </div>
</div>

<nav>
    @foreach (menu('site', '_json') as $menuItem)
        @php $menuItem = $menuItem->translate(app()->getLocale(), 'ru');@endphp
        @if($menuItem->url == "/")
            <a class="{{request()->getRequestUri() == $menuItem->url ? 'active-index':''}}" href="{{ $menuItem->url }}">{{ $menuItem->title}}</a>
        @else
            <a class="{{request()->getRequestUri() == $menuItem->url ? 'active-link':''}}" href="{{ $menuItem->url }}">{{ $menuItem->title}}</a>
        @endif
    @endforeach
</nav>
