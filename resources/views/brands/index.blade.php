@extends('layouts.app')
@section('content')

    <div class="projects" data-aos="flip-up">
        <div class="container">
            <div class="title">
                <h1>{{ $model->title }}</h1>
            </div>
            <div class="projects-discription text-center">
                <p>{!! $model->content !!}</p>
            </div>
        </div>
    </div>
    <div class="brands">
        <div class="container">
            <div class="row">
                @foreach($brands as $k => $v)
                    <div class="col-xl-3 col-md-4 col-12" data-aos="zoom-in">
                        <div class="brand-content">
                            <img src="{{ asset('storage/'.$v->image)}}" alt="">
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
