@extends('layouts.app')
@section('content')

    <div class="projects">
        <div class="container">
            <div class="title" data-aos="fade-up">
                <h1>{{ $model->title }}</h1>
            </div>
            <div class="projects-discription text-center">
                <p>{!! $model->content !!}</p>
            </div>
        </div>
    </div>

    @php $m=0 @endphp
    @foreach($projects as $k => $v)
        @php $m++; $types = $v->types; @endphp
        @if($m % 2 == 1)
            <div class="univer-block" id="p{{$v->id}}">
                <div class="row w-100 m-0">
                    <div class="col-xl-6 col-lg-6">
                        <div class="container">
                            <div class="univer-left-block"  data-aos="fade-zoom">
                                <h2>{{$v->name}}</h2>
                                <p>{{$v->content}}</p>
                                <button class="btn btn-danger btn-service" data-toggle="modal"
                                        data-target="#requestProjectModal" onclick="saveProjectID({{$v->id}})">
                                    @lang('messages.заказать подобный проект')</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 p-0" data-aos="fade-right">
                        <div class="univer-right-block" style='background-image: url("{{ Voyager::image($v->image)}}");'>
                            <div class="univer-right-head">
                                @foreach($types as $type)
                                    @php $type = $type->translate(app()->getLocale()); @endphp
                                    <div class="univer-right-content d-flex align-items-center">
                                        <img src="{{ asset('storage/'.$type->image)}}" alt="">
                                        <h5>{{$type->content}}</h5>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        @else
            <div class="ministr-block" id="p{{$v->id}}">
                <div class="row w-100 m-0">
                    <div class="col-xl-6 col-lg-6 p-0" data-aos="fade-zoom">
                        <div class="ministr-left-block" style='background-image: url("{{Voyager::image($v->image)}}");'>
                            <div class="ministr-left-head">
                                @foreach($types as $type)
                                    @php $type = $type->translate(app()->getLocale()); @endphp
                                    <div class="ministr-left-content d-flex align-items-center">
                                        <img src="{{ asset('storage/'.$type->image)}}" alt="">
                                        <h5>{{$type->content}}</h5>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="container">
                            <div class="ministr-right-block">
                                <h2>{{$v->name}}</h2>
                                <p>{{$v->content}}</p>
                                <button class="btn btn-danger btn-service" data-toggle="modal"
                                        data-target="#requestProjectModal" onclick="saveProjectID({{$v->id}})">
                                    @lang('messages.заказать подобный проект')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach

@endsection
