<div class="modal fade" id="requestClientModal" tabindex="-1" role="dialog" aria-labelledby="requestClientModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestClientModalLabel">@lang('messages.Станьте нашим клиентом')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">@lang('messages.ФИО'):</label>
                        <input type="text" class="form-control" id="request_fio" name="fio">

                        <label for="message-text" class="col-form-label">@lang('messages.Компания'):</label>
                        <input class="form-control" id="request_company" name="company">

                        <label for="message-text" class="col-form-label">@lang('messages.Лица'):</label>
                        <select class="form-control" id="request_face"  name="face">
                            <option value="1">@lang('messages.Физического Лица')</option>
                            <option value="2">@lang('messages.Юридического Лица')</option>
                        </select>

                        <label for="message-text" class="col-form-label">@lang('messages.Телефон'):</label>
                        <input class="form-control" id="request_telephone" name="telephone" placeholder="@lang('messages.Например'): +77087772757">

                        <label for="message-text" class="col-form-label">@lang('messages.Комментарии'):</label>
                        <textarea class="form-control" id="request_comment" name="comment"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-service"  id="clientRequestButton">@lang('messages.Отправить')</button>
            </div>
        </form>
    </div>
</div>





<div class="modal fade" id="requestServiceModal" tabindex="-1" role="dialog" aria-labelledby="requestServiceModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestServiceModalLabel">@lang('messages.Заказать услугу')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <input  type="hidden" class="form-control" id="request_service_id" name="service_id" >

                    <label for="recipient-name" class="col-form-label">@lang('messages.Имя'):</label>
                    <input type="text" class="form-control" id="request_service_name" name="name">

                    <label for="message-text" class="col-form-label">@lang('messages.Телефон'):</label>
                    <input class="form-control" id="request_service_telephone" name="telephone" placeholder="@lang('messages.Например'): +77087772757">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-service"  id="serviceRequestButton">@lang('messages.Заказать')</button>
            </div>
        </form>
    </div>
</div>




<div class="modal fade" id="requestVacancyModal" tabindex="-1" role="dialog" aria-labelledby="requestVacancyModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestVacancyModalLabel">@lang('messages.Подать заявку')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <input type="hidden" class="form-control" id="request_vacancy_id" name="vacancy_id" >

                    <label for="recipient-name" class="col-form-label">@lang('messages.Имя'):</label>
                    <input type="text" class="form-control" id="request_vacancy_name" name="name">

                    <label for="message-text" class="col-form-label">@lang('messages.Телефон'):</label>
                    <input class="form-control" id="request_vacancy_telephone" name="telephone" placeholder="@lang('messages.Например'): +77087772757">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-service"  id="vacancyRequestButton">@lang('messages.Отправить')</button>
            </div>
        </form>
    </div>
</div>



<div class="modal fade" id="requestProjectModal" tabindex="-1" role="dialog" aria-labelledby="requestProjectModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestVacancyModalLabel"> @lang('messages.Заказать подобный проект')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <input type="hidden" class="form-control" id="request_project_id" name="project_id" >

                    <label for="recipient-name" class="col-form-label">@lang('messages.Имя'):</label>
                    <input type="text" class="form-control" id="request_project_name" name="name">

                    <label for="message-text" class="col-form-label">@lang('messages.Телефон'):</label>
                    <input class="form-control" id="request_project_telephone" name="telephone" placeholder="@lang('messages.Например'): +77087772757">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-service"  id="projectRequestButton">@lang('messages.Заказать')</button>
            </div>
        </form>
    </div>
</div>
